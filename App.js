import React from 'react';
import {StyleSheet, View} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import Ripple from "./src/6. Let's Animate/Ripple";
import {Mail} from "./src/6. Let's Animate/Mail";
import Opacity from './src/2. Animating Properties/Opacity';
import Translate from './src/2. Animating Properties/Translate';
import Scale from './src/2. Animating Properties/Scale';
import WidthHeight from './src/2. Animating Properties/WidthHeight';
import Timing from './src/3. Animated Functions/Timing';
import Spring from './src/3. Animated Functions/Spring';
import Parallel from './src/4. Combining Animations/Parallel';
import Sequence from './src/4. Combining Animations/Sequence';
import Stagger from './src/4. Combining Animations/Stagger';
import Delay from './src/4. Combining Animations/Delay';
import Interpolate from './src/5. Interpolate';
import Example from './src/Example';

const App = () => {
  return (
    <View style={styles.root}>
      <Example />
      {/* <Opacity /> */}
      {/* <Translate /> */}
      {/* <Scale /> */}
      {/* <WidthHeight /> */}
      {/* <Timing /> */}
      {/* <Spring /> */}
      {/* <Parallel /> */}
      {/* <Sequence /> */}
      {/* <Stagger /> */}
      {/* <Delay /> */}
      {/* <Interpolate /> */}
      {/* <Ripple /> */}
      {/* <Mail /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: Colors.lighter,
    paddingTop: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;
