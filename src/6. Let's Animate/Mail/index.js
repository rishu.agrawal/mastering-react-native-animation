// https://lottiefiles.com/9844-loading-40-paperplane
import React, {useRef, useEffect} from 'react';
import {StyleSheet, View, Animated} from 'react-native';

export const Mail = () => {
  const animatedRef = useRef(new Animated.Value(0));

  const animate = () => {
    Animated.loop(
      Animated.timing(animatedRef.current, {
        toValue: 4,
        duration: 3000,
        useNativeDriver: true,
      }),
    ).start();
  };

  /**
   *
   * @param {Array} outputRange returns Interpolated style values
   */
  const getInterpolation = (outputRange) =>
    animatedRef.current.interpolate({
      inputRange: [0, 1, 2, 3, 4],
      outputRange,
    });

  const cloud1Styles = {
    opacity: getInterpolation([0, 0.5, 1, 0.5, 0]),
    transform: [
      {
        translateX: getInterpolation([0, 37, 74, 111, 150]),
      },
    ],
  };

  const cloud2Styles = {
    opacity: getInterpolation([0, 0.5, 1, 0.5, 0]),
    transform: [
      {
        translateX: getInterpolation([0, -37, -74, -111, -150]),
      },
    ],
  };

  const mailStyles = {
    transform: [
      {rotate: getInterpolation(['20deg', '40deg', '20deg', '40deg', '20deg'])},
      {translateY: getInterpolation([0, -20, 0, -20, 0])},
    ],
  };

  useEffect(() => {
    setTimeout(() => {
      animate();
    }, 1000);
  }, []);

  return (
    <View style={styles.root}>
      <Animated.Image
        style={[styles.cloud, styles.cloud1, cloud1Styles]}
        source={require('../../assets/images/cloud.png')}
      />
      <Animated.Image
        style={[styles.mail, mailStyles]}
        source={require('../../assets/images/mail.png')}
      />
      <Animated.Image
        style={[styles.cloud, styles.cloud2, cloud2Styles]}
        source={require('../../assets/images/cloud.png')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    width: 200,
    height: 200,
  },
  cloud: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    tintColor: '#CFD2D2',
  },
  cloud1: {
    alignSelf: 'flex-start',
  },
  cloud2: {
    alignSelf: 'flex-end',
  },
  mail: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    tintColor: '#5D69F7',
    alignSelf: 'center',
    marginVertical: -28,
    transform: [{rotate: '30deg'}],
  },
});
