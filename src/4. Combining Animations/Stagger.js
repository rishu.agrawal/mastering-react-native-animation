import React, {useRef} from 'react';
import {
  StyleSheet,
  View,
  Animated,
  TouchableWithoutFeedback,
} from 'react-native';

const Stagger = () => {
  const colorAnimation = useRef(new Animated.Value(0));
  const scaleAnimation = useRef(new Animated.Value(1));

  const animate = () => {
    Animated.stagger(200, [
      Animated.timing(colorAnimation.current, {
        toValue: 1,
        duration: 500,
      }),
      Animated.timing(scaleAnimation.current, {
        toValue: 2,
        duration: 300,
      }),
    ]).start();
  };

  const backgroundInterpolate = colorAnimation.current.interpolate({
    inputRange: [0, 1],
    outputRange: ['#ff5722', '#57ff22'],
  });

  const animatedStyles = {
    backgroundColor: backgroundInterpolate,
    transform: [{scale: scaleAnimation.current}],
  };

  return (
    <View>
      <TouchableWithoutFeedback onPress={animate}>
        <Animated.View style={[styles.box, animatedStyles]} />
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    width: 100,
    height: 100,
    backgroundColor: '#ff5722',
  },
});
export default Stagger;
