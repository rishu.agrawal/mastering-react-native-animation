import React, {useRef} from 'react';
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Animated,
  Easing,
} from 'react-native';

const Timing = () => {
  const animation = useRef(new Animated.Value(0));

  const animate = () => {
    Animated.timing(animation.current, {
      toValue: 300,
      duration: 1200,
      useNativeDriver: true,
      // easing: Easing.back(5),
      // easing: Easing.bounce,
      // easing: Easing.elastic(3),
      // easing: Easing.bezier(0.06, 1, 0.7, 0.16),
    }).start(() => animation.current.setValue(0));
  };

  const animatedStyles = {
    transform: [
      {
        translateY: animation.current,
      },
    ],
  };

  return (
    <View>
      <TouchableWithoutFeedback onPress={animate}>
        <Animated.View style={[styles.box, animatedStyles]} />
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    width: 100,
    height: 100,
    backgroundColor: '#ff5722',
  },
});

export default Timing;
