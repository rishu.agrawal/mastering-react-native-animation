import React, {useRef} from 'react';
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Animated,
} from 'react-native';

const Spring = () => {
  const animation = useRef(new Animated.Value(1));

  const animate = () => {
    Animated.spring(animation.current, {
      toValue: 2,
      friction: 2,
      tension: 100,
      useNativeDriver: true,
    }).start(() => {
      Animated.timing(animation.current, {
        toValue: 1,
        duration: 100,
        useNativeDriver: true,
      }).start();
    });
  };

  const animatedStyles = {
    transform: [
      {
        scale: animation.current,
      },
    ],
  };

  return (
    <View>
      <TouchableWithoutFeedback onPress={animate}>
        <Animated.View style={[styles.box, animatedStyles]} />
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    width: 100,
    height: 100,
    backgroundColor: '#ff5722',
  },
});

export default Spring;
