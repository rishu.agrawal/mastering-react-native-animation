import React, {useRef} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableWithoutFeedback,
  Animated,
} from 'react-native';

const Scale = () => {
  const animation = useRef(new Animated.Value(1));

  const animate = () => {
    Animated.timing(animation.current, {
      toValue: 2,
      duration: 1200,
      useNativeDriver: true,
    }).start(() => animation.current.setValue(1));
  };

  const animatedStyles = {
    transform: [
      {
        scaleX: animation.current,
      },
      {
        scaleY: animation.current,
      },
    ],
  };

  return (
    <View>
      <TouchableWithoutFeedback onPress={animate}>
        <Animated.View style={[styles.box, animatedStyles]}>
          <Text>Hello World</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    width: 100,
    height: 100,
    backgroundColor: '#ff5722',
  },
});

export default Scale;
