import React, {useRef} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableWithoutFeedback,
  Animated,
} from 'react-native';

const WidthHeight = () => {
  const animation = useRef(new Animated.Value(100));

  const animate = () => {
    Animated.timing(animation.current, {
      toValue: 150,
      duration: 1200,
    }).start(() => animation.current.setValue(100));
  };

  const animatedStyles = {
    width: animation.current,
    height: animation.current,
  };

  return (
    <View>
      <TouchableWithoutFeedback onPress={animate}>
        <Animated.View style={[styles.box, animatedStyles]}>
          <Text>Hello World</Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    width: 100,
    height: 100,
    backgroundColor: '#ff5722',
  },
});

export default WidthHeight;
