import React, {useRef} from 'react';
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Animated,
} from 'react-native';

const Translate = () => {
  const animation = useRef(new Animated.Value(0));

  const animate = () => {
    Animated.timing(animation.current, {
      toValue: 100,
      duration: 1200,
      useNativeDriver: true,
    }).start(() => animation.current.setValue(0));
  };

  const animatedStyles = {
    transform: [
      {
        translateX: animation.current,
      },
      {
        translateY: animation.current,
      },
    ],
  };

  return (
    <View>
      <TouchableWithoutFeedback onPress={animate}>
        <Animated.View style={[styles.box, animatedStyles]} />
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    width: 100,
    height: 100,
    backgroundColor: '#ff5722',
  },
});

export default Translate;
