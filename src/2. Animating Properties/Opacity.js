import React, {useRef} from 'react';
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Animated,
} from 'react-native';

const Opacity = () => {
  const animation = useRef(new Animated.Value(1));

  const animate = () => {
    Animated.timing(animation.current, {
      toValue: 0.1,
      duration: 350,
      useNativeDriver: true,
    }).start(() => {
      Animated.timing(animation.current, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true,
      }).start();
    });
  };

  const animatedStyles = {
    opacity: animation.current,
  };

  return (
    <View>
      <TouchableWithoutFeedback onPress={animate}>
        <Animated.View style={[styles.box, animatedStyles]} />
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    width: 100,
    height: 100,
    backgroundColor: '#ff5722',
  },
});

export default Opacity;
