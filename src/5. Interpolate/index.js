import React, {useRef} from 'react';
import {
  StyleSheet,
  View,
  Animated,
  TouchableWithoutFeedback,
} from 'react-native';

const Interpolate = () => {
  const animation = useRef(new Animated.Value(0));

  const animate = () => {
    Animated.timing(animation.current, {
      toValue: 2,
      duration: 500,
      useNativeDriver: true,
    }).start(() => animation.current.setValue(0));
  };

  const animatedInterpolate = animation.current.interpolate({
    inputRange: [0, 1, 2],
    outputRange: [1, 2, 1],
    // extrapolate: 'clamp',
  });

  const animatedStyles = {
    transform: [{scale: animatedInterpolate}],
  };

  return (
    <View>
      <TouchableWithoutFeedback onPress={animate}>
        <Animated.View style={[styles.box, animatedStyles]} />
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    width: 100,
    height: 100,
    backgroundColor: '#ff5722',
  },
});
export default Interpolate;
